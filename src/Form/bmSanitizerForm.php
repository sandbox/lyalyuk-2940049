<?php

namespace Drupal\backup_migrate_sanitizer\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use BackupMigrate\Drupal\Config\DrupalConfigHelper;
use BackupMigrate\Core\Service\ServiceManager;
use BackupMigrate\Core\Plugin\PluginManager;
use BackupMigrate\Core\Main\BackupMigrate;
use BackupMigrate\Core\Destination\BrowserDownloadDestination;
use Drupal\backup_migrate_sanitizer\Source\MySQLiSourceSanitized;

/**
 * Class bmSanitizerForm.
 */
class bmSanitizerForm extends FormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;
  /**
   * Constructs a new bmSanitizerForm object.
   */
  public function __construct(
    EntityTypeManager $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'backup_migrate_sanitizer_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Initialize bam.
    $bam = backup_migrate_get_service_object();

    $form['sanitized_backup'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Sanitized Backup'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $form['sanitized_backup']['source'] = [
      '#type' => 'select',
      '#title' => $this->t('Source'),
      '#options' => ['sanitizedmysqldb' => $this->t('MySQLi')],
    ];

    $form['sanitized_backup']['destination'] = DrupalConfigHelper::getDestinationSelector($bam, t('Destination'));

    $entity_types = $this->entityTypeManager->getDefinitions();

    foreach ($entity_types as $entity_type_id => $entity_type) {

      if (!$entity_type instanceof ContentEntityTypeInterface ) {
        continue;
      }

      $storage = $this->entityTypeManager->getStorage($entity_type_id);
      if (!method_exists($storage, 'getFieldStorageDefinitions')) {
        continue;
      }

      $form[$entity_type_id] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Sanitize `@label` fields', ['@label' => $entity_type->getLabel() ? $entity_type->getLabel() : $entity_type_id]),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#tree' => FALSE,
      ];

      $storage = $this->entityTypeManager->getStorage($entity_type_id);
      $default[$entity_type_id] = FALSE;

      $fieldDefinitions = $storage->getFieldStorageDefinitions();

      foreach ($fieldDefinitions as $id => $fieldDefinition) {

        // Todo. Exclude fields Id, UUID, lancode, type etc.
        $label = method_exists($fieldDefinition, 'getName') ? $fieldDefinition->getName() : $id;
        $form[$entity_type_id][$entity_type_id . '__' . $id] = [
          '#type' => 'checkbox',
          '#title' => $label,
        ];

        $form[$entity_type_id][$entity_type_id . '__' . $id . '__method' ] = [
          '#type' => 'select',
          '#title' => $this->t('Method'),
          '#options' => ['remove' => 'Remove'],
          '#states' => array(
            'visible' => array(
              ':input[name="' . $entity_type_id . '__' . $id . '"]' => array('checked' => TRUE),
            ),
          ),
        ];

      }
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Process'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Create a service locator
    $services = new ServiceManager();

    // Create a plugin manager
    $plugins = new PluginManager($services);

    $tables = $this->getTablesFromFieldNames($form_state->getValues());

    // ToDo. another way to transmit data needed.
    $tempstore = \Drupal::service('user.private_tempstore')->get('backup_migrate_sanitizer');
    $tempstore->set('backup_migrate_sanitizer', $tables);

    $bam = new BackupMigrate($plugins);

    // Allow other modules to alter the object
    \Drupal::moduleHandler()->alter('backup_migrate_service_object', $bam, $options);

    $bam->backup($form_state->getValue('source'), $form_state->getValue('destination'));

  }

  /**
   * Load Tables and column name from DB.
   *
   * @param array $values
   *   All form values from Form_state
   *
   * @return array
   *   Tree of table>fields for DB.
   */
  public function getTablesFromFieldNames(array $values) {
    // Get values to sanitize.
    $sanitizeValues = array_keys(array_filter($values, function ($v, $k) {
      return !empty($k) && $v === 1;
    }, ARRAY_FILTER_USE_BOTH));

    $tables = [];
    foreach ($sanitizeValues as $value) {
      list($entityType, $fieldName) = explode('__', $value);
      $storage = $this->entityTypeManager->getStorage($entityType);
      $dataTable = $storage->getDataTable();
      // ToDo. #1. Add supporting fields in standalone tables.
      // ToDo. #2. Add support of Revisions.
      // ToDo. #3. Provide way of replacing.
      $tables[$dataTable][] = $fieldName;
    }

    return $tables;
  }

}
