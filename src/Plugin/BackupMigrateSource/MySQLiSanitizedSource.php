<?php

namespace Drupal\backup_migrate_sanitizer\Plugin\BackupMigrateSource;

use BackupMigrate\Drupal\EntityPlugins\SourcePluginBase;
use Drupal\backup_migrate_sanitizer\Source\MySQLiSourceSanitized;
use Drupal\Core\Database\Database;

/**
 * Defines an Sanitized database source plugin.
 *
 * @BackupMigrateSourcePlugin(
 *   id = "sanitizedmysqldb",
 *   title = @Translation("Sanitized Mysql Database"),
 *   description = @Translation("Back up the Drupal db."),
 * )
 */
class MySQLiSanitizedSource extends SourcePluginBase {

  /**
   * Get the Backup and Migrate plugin object.
   *
   */
  public function getObject() {
    // Add the default database.
    $info = Database::getConnectionInfo('default', 'default');
    $info = $info['default'];

    // Set a default port if none is set. Because that's what core does.
    $info['port'] = (empty($info['port']) ? 3306 : $info['port']);
    if ($info['driver'] == 'mysql') {
      $conf = $this->getConfig();
      foreach ($info as $key => $value) {
        $conf->set($key, $value);
      }
      // ToDo. another way to transmit data needed.
      $tempstore = \Drupal::service('user.private_tempstore')->get('backup_migrate_sanitizer');

      return new MySQLiSourceSanitized($conf, $tempstore->get('backup_migrate_sanitizer'));
    }

    return null;
  }

}
