<?php

/**
 * @file
 * Contains backup_migrate_sanitizer/src/Source/MySQLiSourceSanitized.php
 */

namespace Drupal\backup_migrate_sanitizer\Source;

use BackupMigrate\Core\Source\MySQLiSource;
use BackupMigrate\Core\File\BackupFileWritableInterface;

/**
 * Class MySQLiSourceSanitized
 * Add sanitizing functionality for MysqlSource
 */
class MySQLiSourceSanitized extends MySQLiSource {

  protected $sanitizeTables;

  public function __construct($init = [], $tables = []) {
    parent::__construct($init);
    $this->sanitizeTables = $tables;
  }

  /**
   *  Get the sql to insert the data for a given table
   *
   * @param \BackupMigrate\Core\File\BackupFileWritableInterface $file
   * @param $table
   */
  protected function _dumpTableSQLToFile(BackupFileWritableInterface $file, $table) {
    // If this is a view, do not export any data
    if (empty($table['engine'])) {
      return 0;
    }

    // Otherwise export the table data.
    $rows_per_line  = 30; //$this->confGet('rows_per_line');//variable_get('backup_migrate_data_rows_per_line', 30);
    $bytes_per_line = 2000; //$this->confGet('bytes_per_line'); variable_get('backup_migrate_data_bytes_per_line', 2000);
    $lines = 0;
    $result = $this->query("SELECT * FROM `". $table['name'] ."`");
    $rows = $bytes = 0;

    // Escape backslashes, PHP code, special chars
    $search = array('\\', "'", "\x00", "\x0a", "\x0d", "\x1a");
    $replace = array('\\\\', "''", '\0', '\n', '\r', '\Z');

    while ($result && $row = $result->fetch_assoc()) {
      // DB Escape the values.
      $items = array();
      foreach ($row as $key => $value) {
        if (in_array($table['name'], array_keys($this->sanitizeTables)) && in_array($key, $this->sanitizeTables[$table['name']])) {
          // ToDo. Implement random data creation.
          $value = '123999123';
        }
        $items[] = is_null($value) ? "null" : "'". str_replace($search, $replace, $value) ."'";

        // @TODO: escape binary data
      }

      // If there is a row to be added.
      if ($items) {
        // Start a new line if we need to.
        if ($rows == 0) {
          $file->write("INSERT INTO `". $table['name'] ."` VALUES ");
          $bytes = $rows = 0;
        }
        // Otherwise add a comma to end the previous entry.
        else {
          $file->write(",");
        }

        // Write the data itself.
        $sql = implode(',', $items);
        $file->write('('. $sql .')');
        $bytes += strlen($sql);
        $rows++;

        // Finish the last line if we've added enough items
        if ($rows >= $rows_per_line || $bytes >= $bytes_per_line) {
          $file->write(";\n");
          $lines++;
          $bytes = $rows = 0;
        }
      }
    }
    // Finish any unfinished insert statements.
    if ($rows > 0) {
      $file->write(";\n");
      $lines++;
    }

    return $lines;

  }

}